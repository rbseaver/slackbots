var assert = require('chai').assert;
var expect = require('chai').expect;
var should = require('should');

var SongParser = require('../js/SongParser');

describe('When parsing lyrics from API', function () {
    var context = {
        "context": "I see a <em>bad</em> <em>moon</em> <em>rising</em>\r\nI see trouble on the way\r\nI see earthquakes and lighting\r\nI see <em>bad</em> times today\r\n\r\nNow don't go 'round tonight\r\nFor it's",
        "instrumental": false,
        "snippet": "I see a bad moon rising\r\nI see trouble on the way\r\nI see earthquakes and lighting\r\nI see bad times today\r\n\r\nNow don't go 'round tonight\r\nFor it's boun...",
        "title": "Bad Moon Rising",
        "url": "http://www.lyricsnmusic.com/emmylou-harris/bad-moon-rising-lyrics/3639655",
        "viewable": false,
        "artist": {
            "name": "Emmylou Harris",
            "url": "http://www.lyricsnmusic.com/emmylou-harris"
        }
    };
    
    it('it should convert results to song', function () {
        var songModel = SongParser.ParseSong(context);
        songModel.Title.should.equal('Bad Moon Rising');
        songModel.Url.should.equal('http://www.lyricsnmusic.com/emmylou-harris/bad-moon-rising-lyrics/3639655');
        songModel.Artist.Name.should.equal('Emmylou Harris');
        songModel.Artist.Url.should.equal('http://www.lyricsnmusic.com/emmylou-harris');
    });
    
    
    it('it should return a collection of songs', () => {
        var songs = SongParser.GetSongsByLyrics('I see a bad moon rising');
        songs.length.should.equal(1);
    });
        
});