"use strict";
var Song_1 = require('./Song');
var Artist_1 = require('./Artist');
function ParseSong(data) {
    var songModel = new Song_1.Song();
    var songArtist = new Artist_1.Artist();
    songArtist.Name = data.artist.name;
    songArtist.Url = data.artist.url;
    songModel.Title = data.title;
    songModel.Url = data.url;
    songModel.LyricsSnippet = data.context;
    songModel.Artist = songArtist;
    return songModel;
}
exports.ParseSong = ParseSong;
function GetSongsByLyrics(lyrics) {
    var songs = [];
    songs.push(new Song_1.Song());
    return songs;
}
exports.GetSongsByLyrics = GetSongsByLyrics;
//# sourceMappingURL=SongParser.js.map