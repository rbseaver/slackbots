import { Song } from './Song';
import { Artist } from './Artist';

export function ParseSong(data: any): Song {
		
	let songModel: Song = new Song();
	let songArtist : Artist = new Artist();
	
	songArtist.Name = data.artist.name;
	songArtist.Url = data.artist.url;
	
	songModel.Title = data.title;
	songModel.Url = data.url;
	songModel.LyricsSnippet = data.context;
	songModel.Artist = songArtist;
	
	return songModel;
}

export function GetSongsByLyrics(lyrics: string): Array<Song> {
	let songs: Array<Song> = [];
	songs.push(new Song());
	return songs;
}