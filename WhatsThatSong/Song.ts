import { Artist } from './Artist';

class Song {
	Title: string;
	Url: string;
	LyricsSnippet: string;
	Artist: Artist;
}

export { Song };